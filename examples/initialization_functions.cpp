#include <unistd.h>

#include "cppcurses.hpp"

int main(int args, char **argv)
{
    // Use this to give the debugger time to attach.
    // usleep(1000000);

    curses::screen &scr = curses::get_screen();
    scr->init()->raw()->noecho();
    std::shared_ptr<curses::window> stdscr = scr->stdscr();
    stdscr->keypad(true);
    stdscr << "Type any character to see it in bold\n";
    stdscr->refresh();
    int ch;
    stdscr >> ch;
    if (ch == curses::key::f(1)) {
        stdscr << "F1 key pressed.";
    }
    else
    {
        stdscr << "The pressed key is ";
        stdscr->bold(true);
        stdscr << ch;
        stdscr->bold(false);
    }
    stdscr << "\nCool, right? Now press any key to quit...";
    scr.refresh();
    stdscr->getch();
    scr.end();
}