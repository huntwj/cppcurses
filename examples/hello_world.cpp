#include "cppcurses.hpp"

int main(int args, char **argv)
{
    curses::screen &scr = curses::get_screen();
    scr.init();
    std::shared_ptr<curses::window> stdscr = scr->stdscr();
    stdscr << "Hello, World!!!";
    stdscr->refresh();
    stdscr->getch();
    scr.end();
}