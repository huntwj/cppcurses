#include "cppcurses.hpp"

std::shared_ptr<curses::window> create_newwin(int lines, int cols, int y, int x)
{
    std::shared_ptr<curses::window> win(curses::get_screen().create_window(lines, cols, y, x));
    win->box()->refresh();
    return win;
}

void destroy_win(std::shared_ptr<curses::window> win)
{
    win->border(' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
    win->refresh();
}

int main(int argc, char **argv)
{
    curses::screen &scr = curses::get_screen();
    scr.init().cbreak();
    std::shared_ptr<curses::window> stdscr = scr.stdscr();
    int startx, starty, width, height;
    int ch;

    stdscr->keypad();       /* I need that nifty F1     */

    height = 3;
    width = 10;
    starty = (scr.lines() - height) / 2;  /* Calculating for a center placement */
    startx = (scr.cols() - width) / 2;    /* of the window                      */
    stdscr << "Press F1 to exit";
    stdscr->refresh();

    std::shared_ptr<curses::window> my_win(create_newwin(height, width, starty, startx));
    while((ch = stdscr->getch()) != curses::key::f(1))
    {
        switch(ch)
        {
            case curses::key::left:
                destroy_win(my_win);
                my_win = create_newwin(height, width, starty,--startx);
                break;
            case curses::key::right:
                destroy_win(my_win);
                my_win = create_newwin(height, width, starty,++startx);
                break;
            case curses::key::up:
                destroy_win(my_win);
                my_win = create_newwin(height, width, --starty,startx);
                break;
            case curses::key::down:
                destroy_win(my_win);
                my_win = create_newwin(height, width, ++starty,startx);
                break;
        }
    }

    scr.end();           /* End curses mode        */
    return 0;
}