#include <unistd.h>

#include "curses_screen.hpp"

#include <curses.h>

namespace curses
{
    curses_screen::curses_screen()
    : _initialized(false)
    {
    }

    curses_screen::~curses_screen()
    {
        this->end();
    }

    screen &curses_screen::init()
    {
        this->_initialized = true;
        ::initscr();
        this->noecho();
        ::nonl();

        return *this;
    }

    screen &curses_screen::raw()
    {
        ::raw();
        return *this;
    }

    screen &curses_screen::noecho()
    {
        ::noecho();
        return *this;
    }

    screen &curses_screen::cbreak()
    {
        ::cbreak();
        return *this;
    }

    screen &curses_screen::end()
    {
        if (this->_initialized) {
            this->_initialized = false;
            endwin();
        }

        return *this;
    }

    int curses_screen::lines() const
    {
        return LINES;
    }

    int curses_screen::cols() const
    {
        return COLS;
    }

    std::shared_ptr<window> curses_screen::stdscr()
    {
        if (!this->_stdscrwin)
        {
            this->_stdscrwin.reset(new curses_window(::stdscr));
        }
        return this->_stdscrwin;
    }

    std::shared_ptr<window> curses_screen::create_window(int lines, int cols, int y, int x)
    {
        return std::shared_ptr<window>(new curses_window(lines, cols, y, x));
    }


    screen &curses_screen::refresh()
    {
        this->stdscr()->refresh();

        return *this;
    }
}