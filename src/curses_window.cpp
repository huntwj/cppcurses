#include "curses_window.hpp"


namespace curses
{
    curses_window::curses_window(WINDOW *win)
    : _win(win)
    {
        ::intrflush(this->_win, false);
    }

    curses_window::curses_window(int lines, int cols, int y, int x)
    : _win(newwin(lines, cols, y, x))
    {
        ::intrflush(this->_win, false);
    }

    curses_window::~curses_window()
    {
        if (this->_win)
        {
            delwin(this->_win);
        }
    }

    window *curses_window::keypad(bool new_state)
    {
        ::keypad(this->_win, new_state);

        return this;
    }

    window *curses_window::refresh()
    {
        wrefresh(this->_win);
        return this;
    }

    window *curses_window::bold(bool new_state)
    {
        if (new_state)
        {
            wattron(this->_win, A_BOLD);
        }
        else
        {
            wattroff(this->_win, A_BOLD);
        }
        return this;
    }


    window *curses_window::mv(int y, int x)
    {
        wmove(this->_win, y, x);
        return this;
    }

    window *curses_window::putch(char ch)
    {
        waddch(this->_win, ch);
        return this;
    }

    window *curses_window::puts(std::string message)
    {
        waddstr(this->_win, message.c_str());
        return this;
    }

    int curses_window::getch()
    {
        int ret_val = wgetch(stdscr);
        return ret_val;
        // return wgetch(this->_win);
    }

    window *curses_window::box(char vs, char hs)
    {
        ::box(this->_win, vs, hs);
        return this;
    }

    window *curses_window::border(char ls, char rs, char ts, char bs, char tl, char tr, char bl, char br)
    {
        ::wborder(this->_win, ls, rs, ts, bs, tl, tr, bl, br);
        return this;
    }

    window &curses_window::operator<<(std::string message)
    {
        this->puts(message);
        return (*this);
    }

    window &curses_window::operator<<(const char ch)
    {
        this->putch(ch);
        return (*this);
    }
}