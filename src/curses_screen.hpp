#pragma once
#ifndef curses_curses_screen_hpp
#define curses_curses_screen_hpp

#include <memory>

#include "screen.hpp"

#include "curses_window.hpp"

namespace curses
{
    class curses_screen : public screen
    {
    public:
        curses_screen();
        ~curses_screen();

        virtual screen &init();
        virtual screen &raw();
        virtual screen &noecho();
        virtual screen &cbreak();
        virtual screen &end();

        virtual int lines() const;
        virtual int cols() const;

        virtual std::shared_ptr<window> stdscr();
        virtual std::shared_ptr<window> create_window(int lines, int cols, int y, int x);

        virtual screen &refresh();
    private:
        bool _initialized;
        std::shared_ptr<curses_window> _stdscrwin;
    };
}
#endif