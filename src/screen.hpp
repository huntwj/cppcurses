#pragma once
#ifndef curses_screen_hpp
#define curses_screen_hpp

#include <string>

namespace curses
{
    class window;

    class screen
    {
    public:
        virtual screen &init() = 0;
        virtual screen &raw() = 0;
        virtual screen &noecho() = 0;
        virtual screen &cbreak() = 0;
        virtual screen &end() = 0;
        virtual int lines() const = 0;
        virtual int cols() const = 0;

        virtual std::shared_ptr<window> stdscr() = 0;
        virtual std::shared_ptr<window> create_window(int lines, int cols, int y, int x) = 0;
        virtual screen &refresh() = 0;

        virtual screen *operator->();

    };

    screen &get_screen();

    namespace key
    {
        const int down  = 0402;
        const int up    = 0403;
        const int left  = 0404;
        const int right = 0405;

        const int f0    = 0410;
        int f(int fkey);
    }
}
#endif