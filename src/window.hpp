#pragma once
#ifndef curses_window_hpp
#define curses_window_hpp

#include <memory>
#include <string>

namespace curses
{
    class window
    {
    public:
        virtual window *refresh() = 0;
        virtual window *bold(bool new_state) = 0;
        virtual window *mv(int y, int x) = 0;

        virtual window *keypad(bool new_state = true) = 0;
        virtual window *putch(char ch) = 0;
        virtual window *puts(std::string message) = 0;
        virtual int getch() = 0;

        virtual window *box(char vs = 0, char hs = 0) = 0;
        virtual window *border(char ls = 0, char rs = 0, char ts = 0, char bs = 0, char tl = 0, char tr = 0, char bl = 0, char br = 0) = 0;
        virtual window &operator<<(std::string message) = 0;
        virtual window &operator<<(char ch) = 0;
    };

    std::shared_ptr<window> &operator<<(std::shared_ptr<window> &lhs, char ch);
    std::shared_ptr<window> &operator<<(std::shared_ptr<window> &lhs, std::string message);
    std::shared_ptr<window> &operator>>(std::shared_ptr<window> &lhs, int &ch);
}

#endif