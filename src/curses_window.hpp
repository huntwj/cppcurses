#pragma once
#ifndef curses_curses_window_hpp
#define curses_curses_window_hpp

#include "window.hpp"

#include <curses.h>

#ifdef getch
#undef getch
#endif
#ifdef border
#undef border
#endif

namespace curses
{
    class curses_window : public window
    {
    public:
        explicit curses_window(WINDOW *win);
        curses_window(int lines, int cols, int y, int x);
        ~curses_window();

        virtual window *refresh();
        virtual window *bold(bool new_state);

        virtual window *mv(int y, int x);

        virtual window *keypad(bool new_state);
        virtual window *putch(char ch);
        virtual window *puts(std::string message);
        virtual int getch();

        virtual window *box(char vs, char hs);
        virtual window *border(char ls, char rs, char ts, char bs, char tl, char tr, char bl, char br);

        virtual window &operator<<(std::string message);
        virtual window &operator<<(char ch);

    private:
        WINDOW *_win;
    };
}
#endif