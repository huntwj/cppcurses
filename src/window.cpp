#include "window.hpp"

namespace curses
{
    std::shared_ptr<window> &operator<<(std::shared_ptr<window> &lhs, const char ch)
    {
        lhs->putch(ch);
        return lhs;
    }

    std::shared_ptr<window> &operator<<(std::shared_ptr<window> &lhs, const std::string message)
    {
        lhs->puts(message);
        return lhs;
    }

    std::shared_ptr<window> &operator>>(std::shared_ptr<window> &lhs, int &ch)
    {
        ch = lhs->getch();
        return lhs;
    }
}
