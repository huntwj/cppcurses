#include "screen.hpp"
#include "curses_screen.hpp"

#include <curses.h>

namespace curses
{
    screen &get_screen()
    {
        static curses_screen scr;
        return scr;
    }

    screen *screen::operator->()
    {
        return this;
    }

    namespace key
    {
        int f(int fkey)
        {
            return f0 + fkey;
        }
    }
}