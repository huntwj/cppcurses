sources = src/screen.cpp \
		  src/window.cpp \
          src/curses_screen.cpp \
          src/curses_window.cpp
headers = src/cppcurses.hpp \
          src/screen.hpp \
          src/window.hpp

curses_test: test/curses_test.cpp libcppcurses.a $(sources) $(headers)
	g++ -g -std=c++11 -o curses_test test/curses_test.cpp -Isrc -L. -lcppcurses -lcurses

libcppcurses.a: $(sources) $(headers)
	g++ -g -std=c++11 -c $(sources)
	ar -rcs libcppcurses.a screen.o window.o curses_window.o curses_screen.o

hello_world: examples/hello_world.cpp
	g++ -g -std=c++11 -o hello_world examples/hello_world.cpp -Isrc -L. -lcppcurses -lcurses

initialization_functions: examples/initialization_functions.cpp
	g++ -g -std=c++11 -o initialization_functions examples/initialization_functions.cpp -Isrc -L. -lcppcurses -lcurses

window_border: examples/window_border.cpp
	g++ -g -std=c++11 -o window_border examples/window_border.cpp -Isrc -L. -lcppcurses -lcurses

clean:
	rm -f hello_world initialization_functions window_border curses_test *.o libcppcurses.a

force_all: clean curses_test hello_world initialization_functions window_border