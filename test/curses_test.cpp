#include "cppcurses.hpp"

#include <memory>

int main(int argc, char **argv)
{
    curses::screen &scr = curses::get_screen();
    scr.init();

    int lines = scr.lines();
    int cols = scr.cols();
    std::shared_ptr<curses::window> win = scr.create_window(lines, cols, 0, 0);

    win->mv(1, 1)->puts("Press any key to continue...");
    win->refresh();
    win->getch();
}